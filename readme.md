
About
=====

This is simple VirtualHost editor for MAMP stack. You can change it and use it wherever
you like. Licensed under WTFPL:

    This program is free software. It comes without any warranty, to
    the extent permitted by applicable law. You can redistribute it
    and/or modify it under the terms of the Do What The Fuck You Want
    To Public License, Version 2, as published by Sam Hocevar. See
    http://sam.zoy.org/wtfpl/COPYING for more details.

Installation
============

Installation is very easy - just copy the following into your Terminal app:

    curl -Ls https://bitbucket.org/r00m/vhemamp/downloads/install.sh | bash

That command downloads a litle bash script that in turn downloads project files and installs
it to MAMP menu.
