<?php
/*
 * Path to the httpd-vhosts.conf
 *
 * This path is used by VirtualHost manager class.
 * Usually the default value is OK.
 */
define('VHOST_CONF', '/Applications/MAMP/conf/apache/extra/httpd-vhosts.conf');

/*
 * Absolute path to the directory where source files are located
 *
 * Used for correct URL linking. If you change this, then you also have to 
 * manually change the path in /js/virtualhosts.js accordingly.
 */
define('ROOT_PATH', '/vhemamp');


include_once 'classes/vhost-manager.php';


// vhost query string is added only when submiting forms
if (isset($_GET['vhost']) && !empty($_GET['vhost']))
{
    try
    {
        $vHostManager = new VHostManager();


        // for creating or updating virtualhosts
        if (isset($_POST['save']))
        {
            // remove submit button from POST
            unset($_POST['save']);


            if (isset($_POST['isNew']))
            {
                // set default port
                if (! isset($_POST['Port']))
                    $port = 80;

                $host = $vHostManager->newHost($_GET['vhost'], $port);

                foreach ($_POST as $key => $value)
                    $host->$key = stripslashes($value);

                $host->isNew = true;
            }
            else // updating existing record
            {
                $host = $vHostManager->getHostByName($_GET['vhost']);

                // we don't know which properties changed, so it's safer to just discard the old ones
                $host->deleteAllProperties();

                foreach ($_POST as $key => $value)
                    $host->$key = stripslashes($value);

                $host->hasChanged = true;
            }
            
        }


        // for deleting virtualhost from httpd-vhosts.conf
        if (isset($_POST['remove']))
        {
            $host = $vHostManager->getHostByName($_GET['vhost']);

            $host->pendingDeletion = true;
        }


        // flush changes to disk
        $vHostManager->commit();
    }
    catch(Exception $e)
    {
        echo "Error: " . $e->getMessage();
    }

    header("Location: /virtualhosts/");
    exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>MAMP</title>
        <script type="text/javascript" src="<?php echo ROOT_PATH; ?>/js/jquery-1.8.1.min.js"></script>
        <script type="text/javascript" src="<?php echo ROOT_PATH; ?>/js/jquery-ui-1.8.23.custom.min.js"></script>
        <script type="text/javascript" src="<?php echo ROOT_PATH; ?>/js/virtualhosts.js"></script>
        <link type="text/css" href="<?php echo ROOT_PATH; ?>/css/ui-lightness/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
        <link type="text/css" href="<?php echo ROOT_PATH; ?>/css/main.css" rel="stylesheet" />
        <!-- MAMP stylesheet -->
        <link type="text/css" href="<?php echo ROOT_PATH; ?>/../css/screen.css" rel="stylesheet" />
    </head>
    <body>
        <div id="contentWrap">
            <div id="content">
                <div class="frame">
                    <h1>Apache virtualhosts editor</h1>
                    <p>Don't forget to <code>sudo vim /etc/hosts</code> when adding or removing virtualhosts, and also to restart your MAMP.</p>
                    <p>VirtualHosts that have unsaved changes are in <span class="unsavedChanges">red</span>.</p>
                    <p><input type="button" name="new" id="newVirtualHost" value="New Virtualhost" /></p>
                    <?php
                    try
                    {
                        $vHostManager = new VHostManager(VHOST_CONF);

                        echo '<h2>Current virtualhosts (<span id="virtualHostCount">' . $vHostManager->hostCount . '</span>)</h2>';

                        if ($vHostManager->hosts)
                        {
                            foreach($vHostManager->hosts as $host)
                            {
                                $hostId = strpos($host->serverName, ".") === false ? $host->serverName : substr($host->serverName, 0, strpos($host->serverName, "."));

                                echo '<form name="' . $host->serverName . '" id="' . $hostId . '" method="post" action="' . ROOT_PATH . '/?vhost=' . $host->serverName . '">';
                                echo "<fieldset>";
                                echo "<legend>{$host->serverName}</legend>";


                                echo '<div class="http-logs">';

                                if ($host->hasAccessLog)
                                    echo '<a href="#">[Access log]</a>';

                                if ($host->hasErrorLog)
                                    echo '<a href="#">[Error log]</a>';

                                echo '</div>';



                                foreach($host->properties() as $key => $value)
                                {
                                    echo '<div class="property">';
                                        echo '<div class="propertyTitle">' . $key . '</div>';
                                        echo '<input type="text" name="' . $key . '" value="' . htmlentities($value) . '" />';
                                        echo '<a href="#" class="deleteVirtualHostProperty">X</a>';
                                    echo '</div>';
                                }
                                
                                echo '<input type="button" class="newProperty" name="new-property" value="New property" />';


                                echo "</fieldset>";
                                echo '<input type="submit" class="left" name="save" value="Save" />';
                                echo '<input type="button" class="right deleteVirtualHost" name="remove" value="Remove" />';
                                echo '<div class="clear"></div>';
                                echo "</form>";
                                
                            }
                        }
                    }
                    catch(Exception $e)
                    {
                        echo "Error: " . $e->getMessage();
                    }
                    ?>
                </div> <!-- frame -->
            </div> <!-- content -->
        </div> <!-- contentWrap -->
    </body>
</html>