
// Absolute path to the directory where source files are located
var ROOT_PATH = '/vhemamp';

// Dialog contents
var newPropertyDialog = 
'<div id="newPropertyDialog"> \
    <p>Property title (CamelCase required):</p> \
    <input type="text" id="propertyTitle" value="" /> \
    <p>Property value:</p> \
    <input type="text" id="propertyValue" value="" /> \
</div>';

var newVirtualHostDialog = 
'<div id="newVirtualHostDialog"> \
    <p>VirtualHost FQDN:</p> \
    <input type="text" id="servername" value="" /> \
    <p>VirtualHost document root (w/ double quotes):</p> \
    <input type="text" id="documentroot" value="" /> \
</div>';


$(document).ready(function() {
   
   // dialog for adding new property to a virtualhost
   $(".newProperty").live('click', function() {
        var formId = $(this).closest("form").attr("id");

        $(newPropertyDialog).dialog({
            modal: true,
            resizable: false,
            title: 'New VirtualHost property',
            zIndex: 1,
            show: 'fade',
            hide: 'fade',
            width: 603,
            height: 270,
            position: 'middle',
            close: function() {
                $("#newPropertyDialog").remove();
            },
            buttons: {
                "Ok": function() {
                    var propertyTitle = $("#propertyTitle").val();
                    var propertyValue = $("#propertyValue").val();

                    if (propertyTitle != "" && propertyValue != "") {
                        $("#" + formId + " fieldset input:last").before(' \
                            <div class="property"> \
                                <div class="propertyTitle">'+propertyTitle+'</div> \
                                <input type="text" name="'+propertyTitle+'" value="'+propertyValue+'" /> \
                                <a href="#" class="deleteVirtualHostProperty">X</a> \
                            </div> \
                        ');

                        $("#"+formId+" fieldset").addClass("unsavedChanges");
                    }

                    $(this).dialog("close");
                },
                "Cancel": function() {
                    $(this).dialog("close");
                }
            }
        });
    });

    // dialog to confirm virtualhost property deletion
    $(".deleteVirtualHostProperty").live('click', function() {
        var formId = $(this).closest("form").attr("id");
        var element = $(this).parent();

        $('<p id="deleteVirtualHostPropertyDialog">Are you sure you want to delete this property?</p>').dialog({
            modal: true,
            resizable: false,
            title: 'Delete VirtualHost property',
            zIndex: 1,
            show: 'fade',
            hide: 'fade',
            width: 350,
            height: 145,
            position: 'middle',
            close: function() {
                $("#deleteVirtualHostPropertyDialog").remove();
            },
            buttons: {
                "Yes": function() {
                    element.animate({ height: 'toggle', opacity: 'toggle' }, 'slow', function() { $(this).remove(); });

                    $("#"+formId+" fieldset").addClass("unsavedChanges");

                    $(this).dialog("close");
                },
                "No": function() {
                    $(this).dialog("close");
                }
            }
        });
    });

    // dialog to confirm virtualhost deletion
    $(".deleteVirtualHost").live('click', function() {
        var form = $(this).closest("form");
        var formId = form.attr("id");

        $('<p id="deleteVirtualHostDialog">Are you sure you want to delete this VirtualHost?</p>').dialog({
            modal: true,
            resizable: false,
            title: 'Delete VirtualHost',
            zIndex: 1,
            show: 'fade',
            hide: 'fade',
            width: 350,
            height: 145,
            position: 'middle',
            close: function() {
                $("#deleteVirtualHostDialog").remove();
            },
            buttons: {
                "Yes": function() {

                    $.post(ROOT_PATH+'/index.php?vhost=' + form.attr("name"), {remove: "Remove"});

                    $("#"+formId).animate({ height: 'toggle', opacity: 'toggle' }, 'slow', function() { $(this).remove(); });

                    $("#virtualHostCount").html(parseInt($("#virtualHostCount").html()) - 1);

                    $(this).dialog("close");
                },
                "No": function() {
                    $(this).dialog("close");
                }
            }
        });
    });

    // dialog for adding new virtualhosts
    $("#newVirtualHost").live('click', function() {

        $(newVirtualHostDialog).dialog({
            modal: true,
            resizable: false,
            title: 'New VirtualHost',
            zIndex: 1,
            show: 'fade',
            hide: 'fade',
            width: 350,
            height: 265,
            position: 'middle',
            close: function() {
                $("#newVirtualHostDialog").remove();
            },
            buttons: {
                "Create": function() {

                    var serverName = $("#servername").val();
                    var documentRoot = $("#documentroot").val();

                    if (serverName != "" && documentRoot != "") {
                        $(".frame h2").after(' \
                            <form name="'+serverName+'" id="'+serverName+'" method="post" action="/virtualhosts/?vhost='+serverName+'" style="display:none"> \
                                <input type="hidden" name="isNew" value="true" /> \
                                <fieldset class="unsavedChanges"> \
                                    <legend>'+serverName+'</legend> \
                                    <div class="property"> \
                                        <div class="propertyTitle">ServerName</div> \
                                        <input type="text" name="ServerName" value="'+serverName+'" /> \
                                        <a href="#" class="deleteVirtualHostProperty">X</a> \
                                    </div> \
                                    <div class="property"> \
                                        <div class="propertyTitle">DocumentRoot</div> \
                                        <input type="text" name="DocumentRoot" value="'+documentRoot.replace(/"/g, "&quot;")
+'" /> \
                                        <a href="#" class="deleteVirtualHostProperty">X</a> \
                                    </div> \
                                    <input type="button" class="newProperty" name="new-property" value="New property" /> \
                                </fieldset> \
                                <input type="submit" class="left" name="save" value="Save" /> \
                                <input type="button" class="right deleteVirtualHost" name="remove" value="Remove" /> \
                                <div class="clear"></div> \
                            </form> \
                        ');

                        $("#"+serverName).fadeIn("slow");
                    }

                    $("#virtualHostCount").html(parseInt($("#virtualHostCount").html()) + 1);

                    $(this).dialog("close");
                },
                "Cancel": function() {
                    $(this).dialog("close");
                }
            }
        });
    });

});