<?php

include_once 'classes/host.php';

/**
 * Virtualhost Manager
 *
 * When instantiated, reads and parses vhost configuration into host objects.
 * Allows to create, edit and delete virtualhosts.
 */
class VHostManager
{
    public $hostCount = 0;     // returns virtualhost count
    public $hosts = null;      // array with all virtualhosts 

    private $_conf = '';       // holds httpd-vhosts.conf content


    function __construct()
    {
        $this->import_hosts();
    }

    /*
     * Parses file contents and creates a virtualhost class
     */
    private function import_hosts()
    {
        $handle = @fopen(VHOST_CONF, "r");

        if ($handle)
        {
            // read all file into buffer
            while (($buffer = fgets($handle, 4096)) !== false)
                $this->_conf .= $buffer;

            if (!feof($handle))
                throw new Exception("Unexpected fgets() fail.");

            fclose($handle);


            // preg_match all virtualhost blocks
            preg_match_all("#<VirtualHost \*:([0-9]+)>(.*?)<\/VirtualHost>#ms", $this->_conf, $matches);


            // instantiate each virtualhost object
            $this->hosts = array();

            foreach($matches[2] as $key => $config)
            {
                $host = new Host($config, $matches[1][$key]);

                $this->hosts[$host->serverName] = $host;
                $this->hostCount++;
            }

            // free memory
            $this->_conf = '';
        }
        else
        {
            throw new Exception("Can't open file.");
        }
    }

    /*
     * Returns requested host object
     *
     * Without any checks if it realy exists :D
     */
    public function getHostByName($name)
    {
        return $this->hosts[$name];
    }

    /*
     * Creates new host
     *
     * Wrap call to Host class's constructor, which needs the data to be in
     * httpd-vhosts.conf format, e.g. "key value\n".
     */
    public function newHost($serverName, $port)
    {
        $this->hosts[$serverName] = new Host("ServerName {$serverName}\n", $port);

        return $this->hosts[$serverName];
    }

    /*
     * Save changes to disk
     *
     * On success reloads config from disk.
     */
    public function commit()
    {
        // open file for reading/writing, fp=0
        $handle = @fopen(VHOST_CONF, "r+");

        if ($handle)
        {
            // loop through all host objects
            foreach ($this->hosts as $host)
            {
                if ($host->isNew)
                {
                    // writes new virtualhost block
                    $this->create_virtualhost($handle, $host);
                }

                if ($host->hasChanged)
                {
                    // updates virtualhost block
                    $this->update_virtualhost($handle, $host);
                }

                if ($host->pendingDeletion)
                {
                    // deletes virtualhost block
                    $this->delete_virtualhost($handle, $host);
                }
            }

            fclose($handle);

            // reload configuration
            $this->import_hosts();
        }
        else
        {
            throw new Exception("Can't write to file.");
        }
    }

    /*
     * Creates virtualhost directives
     */
    private function create_virtualhost($handle, $host)
    {
        $this->virtualhost_file_io($handle, $host, "create");
    }

    /*
     * Updates virtualhost directives
     */
    private function update_virtualhost($handle, $host)
    {
        $this->virtualhost_file_io($handle, $host, "update");
    }

    /*
     * Deletes virtualhost from httpd config
     */
    private function delete_virtualhost($handle, $host)
    {
        $this->virtualhost_file_io($handle, $host, "delete");
    }

    /*
     * Commits requested virtualhost changes to disk
     *
     * If we are creating new virtualhost then we seek to the end of file
     * and then dump the host object.
     * 
     * If we are updating OR deleting virtualhost directives then first search
     * for correct virtualhost directives and then do the requested actions.
     */
    private function virtualhost_file_io($handle, $host, $action)
    {
        $block_pos = 0;
        $block_status = 0; // 0 - not found, 1 - in progress, 2 - block has been read

        $data_after_block = '';

        if ($action == "create")
        {
            // seek to the end of file and append newline
            fseek($handle, -1, SEEK_END);
            fwrite($handle, "\n");
        }
        else
        {
            // update/delete
            while (($buffer = fgets($handle, 4096)) !== false)
            {
                if (strpos($buffer, "<VirtualHost") !== false && $block_status == 0)
                    $block_pos = ftell($handle) - strlen($buffer); // begining of the line

                if (strpos($buffer, "ServerName {$host->initialServerName}") !== false && $block_status == 0)
                    $block_status = 1;

                if (strpos($buffer, "</VirtualHost>") !== false && $block_status == 1)
                {
                    $block_status = 2;

                    continue;
                }

                // read the rest of the file (after virtualhost block)
                if ($block_status == 2)
                {
                    $data_after_block .= $buffer;
                }
            }

            // delete data after block pos
            ftruncate($handle, $block_pos - 1);
            fseek($handle, $block_pos - 1);
        }
        

        // write changes
        if ($action != "delete")
            fwrite($handle, $host);
            

        // write any data that follows this block
        fwrite($handle, $data_after_block);

        // free mem
        $data_after_block = '';
    }

}