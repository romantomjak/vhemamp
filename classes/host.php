<?php

/**
 * Virtualhost object
 *
 * Objects of this class are manipulated when outputting or writing to disk.
 */
class Host
{
    // http logs
    public $hasErrorLog = false;
    public $hasAccessLog = false;

    // object status
    public $hasChanged = false;
    public $pendingDeletion = false;
    public $isNew = false;

    public $initialServerName = null; // holds inital servername for IO operations (unique identifier)

    protected $_properties = array(); // array to hold virtualhost directives

    private $_conf = array();         // holds original directives (passed when instantiating object)


    function __construct($config, $port)
    {
        $this->_conf = explode("\n", $config);

        $this->_properties['Port'] = $port;

        $this->load_properties();
    }

    /*
     * Parses config and sets corresponding key/value pairs
     */
    private function load_properties()
    {
        // loop through each config line
        foreach($this->_conf as $line)
        {
            $line = trim($line);

            if (empty($line))
                continue;

            list($key, $value) = explode(" ", $line, 2);


            if ($key == 'ErrorLog')
                $this->hasErrorLog = true;

            if ($key == 'CustomLog')
                $this->hasAccessLog = true;

            if ($key == 'ServerName')
                $this->initialServerName = $value;


            $this->_properties[$key] = $value;
        }
    }

    /*
     * Overloading PHP's magic method __get to use our property array
     */
    public function __get($var)
    {
        // translate first lover character into upper so that property calls look nice
        $var[0] = strtoupper($var[0]);

        if (isset($this->_properties[$var]))
            return $this->_properties[$var];
        else
            return false;
    }

    /*
     * Overloading PHP's magic method __set to use our property array
     */
    public function __set($var, $val)
    {
        // translate first lover character into upper so that property gets assigned correctly
        $var[0] = strtoupper($var[0]);

        $this->_properties[$var] = $val;
    }

    /*
     * Overloading object's toString() method
     *
     * Used when dumping object to disk
     */
    public function __toString()
    {
        $host = "\n<VirtualHost *:{$this->_properties['Port']}>\n";

        foreach($this->_properties as $key => $value)
            if ($key != "Port")
                $host .= "    {$key} {$value}\n";

        $host .= "</VirtualHost>\n";

        return $host;
    }

    /*
     * Returns current host properties
     */
    public function properties()
    {
        return $this->_properties;
    }

    /*
     * Deletes all properties except port
     *
     * Used when updating virtualhost directives. Not sure which properties changed, so it's
     * safer to delete all of them and recreate from $_POST array.
     */
    public function deleteAllProperties()
    {
        $port = $this->_properties['Port'];

        $this->_properties = array("Port" => $port);
    }

}
